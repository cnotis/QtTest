#-------------------------------------------------
#
# Project created by QtCreator 2017-11-15T11:29:11
#
#-------------------------------------------------
QT_VERSION = 4.8.6

QT       += core

QT       -= gui

TARGET = QtHello
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
